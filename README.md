#   SSH as Proxy Sock 5 - SAPS5

Avec SSH on peut faire plein de choses. Du terminal bien sur mais aussi avoir un déport X, un système de fichier distant ou encore mieux, un proxy socks 5.

#   Etape de configuration

Terminal utilisé : `GNU bash, version 5.0.17(1)-release (x86_64-pc-linux-gnu)`

```bash
export CONFIG_NAME=aConfigurationName
export HOSTNAME=example.yanisallouch.com
export PORT=22
export PORT_PROXY=9090
export USERNAME=login@example.yanisallouch.com

echo "Host $CONFIG_NAME
        HostName $HOSTNAME
        Port $PORT
        User $USERNAME" >> ~/.ssh/config
```
#   Démarrer le proxy

```bash
ssh -D $PORT_PROXY -q -C -N $CONFIG_NAME
```

#   Utilisation

- Ajouter dans le logiciel voulu le proxy de type SOCKS 5 suivant :
    - `127.0.0.1:$PORT_PROXY`

#   Exemple avec Firefox

Pour avoir une **nouvelle session** firefox avec le précédent proxy mis en place, il faut faire les étapes suivantes : 
- Faire un nouveau profil en CLI avec `firefox -P new`,
- Dans les paramètres réseau, section proxy socks 5, dans le champs IP, indiquez `127.0.0.1`, dans le champs PORT, indiquez `9090`.

Capture d'écran du résultat :

![Capture d'écran du résultat des paramètres réseau complété dans firefox](./capture-ecran-firefox-proxy-socks-5.png "Capture d'écran des paramètres réseau firefox")

#   Plus d'informations

[https://doc.ubuntu-fr.org/ssh](https://doc.ubuntu-fr.org/ssh)
